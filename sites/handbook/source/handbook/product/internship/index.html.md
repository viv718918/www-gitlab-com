---
layout: handbook-page-toc
title: Product Internship - Best Practices
---

This document describes best practices for [internship for learning](/handbook/people-group/promotions-transfers/#internship-for-learning) with Product.

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Interning for Learning - Product Best Practices

Internships are a great way for a GitLab team member to learn about being a Product Manager at GitLab. Use this guide for planning and executing an internship under an IC PM who acts as the 'Intern Mentor'.

## Planning the internship

The [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/) at GitLab offers a systematic way to scope Product Internships for learning. While not mandatory, it is helpful to select one of the tracks to focus on for the internship. Available tracks:

- [Validation Track](https://about.gitlab.com/handbook/product-development-flow/#validation-track) - PM Intern will learn how to develop business cases for new ideas based on potential customer problems. This may also include the solution validation workflow where the PM Intern will collaborate with UX to create designs for solutions to validated problems.
- [Build Track](https://about.gitlab.com/handbook/product-development-flow/#build-track) - PM Intern will learn how to break down issues and collaborate with engineering to turn UX designs into working software. 

Selection of which track to pursue for the internship should be based on the goals of the PM intern.

## Pre-internship tasks

1. Get manager approval from the intern’s existing manager, identify an IC PM to be the ‘Intern Mentor’, identify the IC’s manager as the ‘Intern Manager’ and define a start date. Set an expectation with the intern’s current manager that the intern will give > 40% of their time to the internship.
1. Create an issue in the [Training project](https://gitlab.com/gitlab-com/people-group/Training) using the **Internship for learning** template. Check out this [issue](https://gitlab.com/gitlab-com/people-group/Training/-/issues/831) as an example.
1. Schedule a weekly recurring 1:1 with the Intern Mentor to foster regular communication regarding intern role and responsibilities. Create a notes doc using the [internship agenda template](https://docs.google.com/document/d/1NnCo8iNtLkBAZH6FnTHjjc1S0UVCcncYGHk3wFnEtTg/edit#heading=h.5mqqjbquuysm). This document will be used to document the plan, progress, and take notes during weekly 1:1s. Check out this [weekly agenda](https://docs.google.com/document/d/1IczVqzZ79KLUq8EqPNSgVk3WgyMmmFnC3IgvAMAzL3o/edit#) as an example.
1. Before the start date, provide the intern with resources such as books and articles relating to Product Management. The Medium Blog [A Living List Of Product Management Resources You’ll Want To Bookmark](https://medium.com/infinitypm/a-living-list-of-product-management-resources-youll-want-to-bookmark-c80b45aa1026) provides a comprehensive list of books, courses, templates, podcasts and tools for PM interns to explore.
1. Add the intern to the email groups, gitlab groups, relevant slack channels and recurring meetings.
1. Discuss workload with current manager in 1:1s to ensure workload percentage between both roles is balanced.
1. Before the start date of the internship, prepare a detailed plan and document it in training issue. Make sure to define actionable results for the intern to focus on and set incremental deadlines.
1. The Intern Manager and Mentor will prioritize the intern like an additional team member and provide regular feedback and guidance.


